<?php

require_once("coursefeed.php");
require_once("template/article.php");
require_once("http.php");

$coursefeed = new CourseFeed();

include('base.php'); // base template

if ($_SERVER['REQUEST_METHOD'] == "GET") {
    startblock('content');
    if (http\has_parameter($_GET, "id")) {
        $articles=array($_GET['id']);
        template\article\renderDeleteForm($articles);
    }
    endblock('content');
}

function redirectToArticleList() {
    header("Location: article.php");
}

if ($_SERVER['REQUEST_METHOD'] == "POST") {
    startblock('content');
    if (http\has_parameter($_POST, "articles")) {
        $articles=$_POST['articles'];
    	if (http\has_parameter($_POST, "delete")) {
	        $article = $coursefeed->deleteArticles($articles);
            redirectToArticleList();
    	} else {
	    	template\article\renderDeleteForm($articles);
    	}
    }

    endblock('content');
}




?>





