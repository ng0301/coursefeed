<?php

require_once("coursefeed.php");
require_once("template/article.php");
require_once("http.php");

$coursefeed = new CourseFeed();

include('base.php'); // base template

if ($_SERVER['REQUEST_METHOD'] == "GET") {
    startblock('content');

    if (http\has_parameter($_GET, "id")) {
        $article = $coursefeed->getArticle($_GET["id"]);
        template\article\renderEditForm($article);
    } else {
        $courses = $coursefeed->getCourse();
        $categories = $coursefeed->getCategory();
        template\article\renderInsertForm($courses, $categories);
    }

    endblock('content');
}

function redirectToArticle($id) {
    header("Location: article.php?id=" . $id);
}

if ($_SERVER['REQUEST_METHOD'] == "POST") {
    if (http\has_parameter($_POST, "id")) {
        $id = $_POST["id"];
        if (http\has_parameter($_POST, "content")) {
            $content = $_POST["content"];
            //2013.11.14 added by 이정훈 - date관련 추가
            $startdate = $enddate = null;
            $startdate = $_POST["startdate"];
            $enddate = $_POST["enddate"];
            $coursefeed->editArticle($id, $content, $startdate, $enddate);
            //2013.11.15 modified by 이정훈 - 임시로 주석처리. sent by 에러남 ! 수정요망.
            redirectToArticle($id);
        }
    } else {
        if (http\has_parameter($_POST, "content")) {
            $content = $_POST["content"];
            if (http\has_parameter($_POST, "title")) {
                $title = $_POST["title"];
                $category_id = $_POST["category"];
                $course_id = $_POST["course"];

                //2013.11.14 added by 이정훈 - date관련 추가
                $startdate = $enddate = null;
                $startdate = $_POST["startdate"];
                $enddate = $_POST["enddate"];
                $id = $coursefeed->insertArticle($title,$category_id,$course_id, $content, $startdate, $enddate);
                redirectToArticle($id);
            }
        }
    }
}

?>
