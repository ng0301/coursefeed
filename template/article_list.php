<? namespace template\article;

function query_string($dict, $diff=array()) {
    $qs = trim(http_build_query(array_merge($dict, $diff)));
    if (strlen($qs) > 0) {
        return "?" . $qs;
    }
    return "";
}

?>

<? function renderListHeader() { ?>
    <h2>Articles <a href="rss.php<?= \http\query_string($_GET, array("month"=>null))?>"><i class = "fa fa-rss-square"></i></a></h2>
<? } ?>

<? function renderList($articles, $page_num) { ?>
    <p>
    <? if (isset($_SESSION['user'])) { ?>
        Why don't you <a href="article_edit.php">write one</a>?
        <!-- add by 이정훈 ADMIN delete 버튼 활성 -->
    <? } else { ?>
        Want to write a article? Login or <a href="join.php">join</a>.
    <? } ?>
    </p>
    <p>View as <a href="<?= \http\query_string($_GET, array('month'=>date('m')))?>">Calendar</a></p>
    <section id="articles">
    <? if (count($articles) > 0) { ?>
        <form action="article_delete.php" method="POST" class="pure-form" id="article-list">
        <? foreach ($articles as $article) { ?>
        <div class="article-entry">
        <? if (isset($_SESSION['user']) and $_SESSION['user']['level'] >= 99) { ?>
            <span><input type="checkbox" name="articles[]" value="<?=$article['id']?>"></span>
        <? } ?>
            <a href="article.php?id=<?=$article["id"]?>"><span class="headline"><?=htmlspecialchars($article["title"]) ?></span><span class="content"><?=htmlspecialchars(mb_substr($article["content"], 0, 30, "utf8")) ?></span>
            <span class="user"><?=$article["user_name"]?></span>
            </a>
            <span class="course"><?=$article["course_name"]?> <span class="category"><?=$article["category_name"]?></span></span>
        </div>
        <? } ?>
        <? if (isset($_SESSION['user']) and $_SESSION['user']['level'] >= 99) { ?>
        <input type="submit" value="delete" class="pure-button pure-button-warning" >
        <? } ?>
        </form>
    <? } else { ?>
        <p>no entry</p>
    <? } ?>
    </section>

    <!-- add by 이정훈, pagenate bar  //class 추가해주세요 !-->
    <ul>
        <? for ($i = max($page_num-2,1); $i <= max($page_num+2,5); $i++) { ?>
            <li><a href="article.php<?= query_string($_GET, array("page" => $i)) ?>"><?=$i?></a></li>
        <? } ?>

    </ul>

<? } ?>
