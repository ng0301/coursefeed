<?php

require_once("coursefeed.php");
require_once("template/article.php");
require_once("template/article_list.php");
require_once("template/calendar.php");
require_once("http.php");

$coursefeed = new CourseFeed();

include('base.php'); // base template

function validMonth($month) {
    return 1 <= $month && $month <= 12;
}

if ($_SERVER['REQUEST_METHOD'] == "GET") {
    startblock('content');
    if (http\has_parameter($_GET, "page")){
        $page_num = $_GET["page"];
    } else {
        $page_num = 1;
    }
    if (http\has_parameter($_GET, "id")) {
        $article = $coursefeed->getArticle($_GET["id"]);
        if ($article["latest_revision"]) {
            template\article\renderEntry($article);
        } else {
            header('HTTP/1.1 404 Not Found');
            template\article\renderNotFound();
        }
    } else if( http\has_parameter($_GET, "nameSearch") || http\has_parameter($_GET, "codeSearch") ) {
        $names = null;
        $codes = null;
        if(http\has_parameter($_GET, "nameSearch"))
            $names = explode(",",$_GET["nameSearch"]);
        if(http\has_parameter($_GET, "codeSearch"))
            $codes = explode(",",$_GET["codeSearch"]);
        $course_ids = $coursefeed->getCourseId($names, $codes);

        $articles = $coursefeed->getArticleList($course_ids);

        template\article\renderListHeader();

        if (http\has_parameter($_GET, "month") && validMonth($_GET["month"])) {
            template\calendar\renderCalendarForm($articles, $_GET["month"]);
        } else {
            template\article\renderList($articles,$page_num);
        }
    } else {
        $articles = $coursefeed->getArticleList(http\maybe_get_parameter($_GET, "course_id"));

        template\article\renderListHeader();

        if (http\has_parameter($_GET, "month") && validMonth($_GET["month"])) {
            template\calendar\renderCalendarForm($articles, $_GET["month"]);
        } else {
            template\article\renderList($articles,$page_num);
        }
    }
    endblock();
}
?>
