<?php

namespace template\joinform;

?>

<?php function renderJoinForm() { ?>
<h2>Sign Up</h2>
<form id="joinform" action="join.php" method="post" class="pure-form pure-form-stacked">
    <label>ID : <input type="text" class="id" name="id" required="required" pattern="^[\w]{4,20}$" /></label>
    <label>PASSWORD : <input type="password" name="password" required="required" pattern="^.{4,20}$" /></label>
    <label>NAME : <input type="text" name="name" required="required" pattern="^.{1,40}$" /></label>
    <label>E-Mail : <input type="email" name="email" required="required" /></label>
    <input type="submit" class="pure-button pure-button-primary joinButton" value="join"/>
</form>
<?php } ?>



<? function renderJoinConfirm() { ?>
<h2>Thank you for Join us !</h2>
<p>Now you just click certificate link.(We just send to you !)</p>
<p>Check it out ;D</p>
<? } ?>