<?php

require_once("db.php");

class CourseFeed {
    private $pdo = null;

    function __construct() {
        $this->pdo = db\connect();
    }

    public function login($id, $password)
    {
        $pdo = $this->pdo;

        $pdo->beginTransaction();

        $query = "SELECT * FROM users WHERE '$id'=user_id and md5('$password')=password";
        $result = $pdo->query($query);
        $rows = $result->fetch();
        //$name = $rows['name'];

        return $rows;
    }

    public function logout()
    {
        $_SESSION = array();
        session_destroy();
    }

    public function insertUser($user_id, $password, $name, $email, $level=1) {
        $pdo = $this->pdo;

        $pdo->beginTransaction();

        $query = "INSERT INTO users (user_id, password, name, email, level) VALUES (:user_id, md5(:password), :name, :email, :level)";
        $stmt = $pdo->prepare($query);
        $stmt->bindValue(':user_id', $user_id);
        $stmt->bindValue(':password', $password);
        $stmt->bindValue(':name', $name);
        $stmt->bindValue(':level', $level);
        $stmt->bindValue(':email', $email);
        $stmt->execute();

        $pdo->commit();
    }

    // getUserLevel : void -> int
    //
    // returns level of current user
    //
    public function getUserLevel() {
        if (isset($_SESSION['user']))
            return $_SESSION['user']['level'];
        else
            return 0;
    }

    // isLoggedIn: void -> boolean
    //
    // returns whether current user logged in or not
    public function isLoggedIn() {
        return isset($_SESSION['user']);
    }

    // isLoggedIn: void -> boolean
    //
    // returns if current user is admin
    public function isAdmin() {
        if (isset($_SESSION['user']) && $_SESSION['user']['level'] >= 99)
            return true;
        else
            return false;
    }

    public function getUserList() {
        $pdo = $this->pdo;
        $stmt = $pdo->prepare("SELECT * FROM users WHERE level < 99");
        $ret = $stmt->execute();

        if ($ret) {
            $obj = $stmt->fetchAll();
            return $obj;
        } else {
            return null;
        }
    }

    public function upvote($article_id) {
        $pdo = $this->pdo;

        $pdo->beginTransaction();

        $query = "INSERT IGNORE INTO upvote (user_id, article_id) VALUES (:user_id, :article_id)";
        $stmt = $pdo->prepare($query);
        $stmt->bindValue(':user_id', $_SESSION['user']['user_id']);
        $stmt->bindValue(':article_id', $article_id);
        $stmt->execute();

        $pdo->commit();
    }

    public function getUpvoteCount($article_id)
    {
        $pdo = $this->pdo;
        $stmt = $pdo->prepare("SELECT COUNT(*) AS upvotes FROM upvote WHERE article_id = :id");
        $stmt->bindValue(":id", $article_id);
        $ret = $stmt->execute();

        if ($ret) {
            $upvote_count = $stmt->fetch();
            return (int)($upvote_count["upvotes"]);
        } else {
            return -1;
        }
    }

    public function canUpvote($user_id, $article_id)
    {
        $pdo = $this->pdo;
        $stmt = $pdo->prepare("SELECT COUNT(*) AS upvotes FROM upvote WHERE article_id = :article_id AND user_id = :user_id LIMIT 1");
        $stmt->bindValue(":article_id", $article_id);
        $stmt->bindValue(":user_id", $user_id);
        $ret = $stmt->execute();

        if ($ret) {
            $upvote_count = $stmt->fetch();
            return (int)($upvote_count["upvotes"]) === 0;
        } else {
            return true;
        }
    }

    public function checkUser($user_id)
    {
        if($user_id == "" || !preg_match("/[\w]{4,20}$/", $user_id))
            return true;

        $pdo = $this->pdo;
        $stmt = $pdo->prepare("SELECT * FROM users WHERE user_id = :id");
        $stmt->bindValue(":id", $user_id);
        $ret = $stmt->execute();

        if($ret)//실행 성공
        {
            if( count($stmt->fetchAll()) > 0 )//중복 존재
                return true;
            else//중복 없음
                return false;
        }
        else//실행 실패
            return true;
    }

    public function insertArticle($title, $category_id, $course_id, $content, $startdate, $enddate) {
        $pdo = $this->pdo;

        $pdo->beginTransaction();

        //2013.11.11 added by 이정훈 - course와 category를 속성 추가.
        //2013.11.14 added by 이정훈 - date 관련 추가.
        $query = "INSERT INTO article (title,category_id,course_id,user_id) VALUES (:title,:category_id,:course_id,:user_id)";
        $stmt = $pdo->prepare($query);
        $stmt->bindValue(':title', $title);
        $stmt->bindValue(':category_id', $category_id);
        $stmt->bindValue(':course_id', $course_id);
        $stmt->bindValue(':user_id', $_SESSION['user']['user_id']);
        $stmt->execute();

        $article = $pdo->query("SELECT id FROM article ORDER BY id DESC LIMIT 1");
        $rows = $article->fetch();
        $id = $rows["id"];

        $pdo->commit();

        $this->editArticle($id, $content, $startdate, $enddate);

        return $id;
    }

    public function getArticle($id) {
        $pdo = $this->pdo;
        $stmt = $pdo->prepare("SELECT article.id, article.title, users.name AS user_name, users.user_id, course.name AS course_name, category.name AS category_name
                                FROM article JOIN users JOIN course JOIN category 
                                ON article.user_id = users.user_id AND article.course_id = course.id AND article.category_id = category.id
                               WHERE article.id = :id LIMIT 1");
        $stmt->bindValue(":id", $id);
        $ret = $stmt->execute();

        if ($ret) {
            $obj = $stmt->fetch();

            $stmt = $pdo->prepare("SELECT * FROM revision WHERE article_id = :id ORDER BY id ASC");
            $stmt->bindValue(":id", $obj["id"]);
            $ret = $stmt->execute();

            if ($ret) {
                $obj["revisions"] = $stmt->fetchAll();
                $obj["latest_revision"] = array_pop($obj["revisions"]);
            }

            $obj["upvotes"] = $this->getUpvoteCount($id);
            $obj["can_upvote"] = $this->isLoggedIn() && $this->canUpvote($_SESSION['user']['user_id'], $id);
        } else {
            $obj = null;
        }

        return $obj;
    }

    public function getCourse() {
        $pdo = $this->pdo;
        $stmt = $pdo->prepare("SELECT * FROM course");
        $ret = $stmt->execute();

        if ($ret) {
            $obj = $stmt->fetchAll();
            return $obj;
        } else {
            return null;
        }
    }


    public function getCategory() {
        $pdo = $this->pdo;
        $stmt = $pdo->prepare("SELECT * FROM category");
        $ret = $stmt->execute();

        if ($ret) {
            $obj = $stmt->fetchAll();
            return $obj;
        } else {
            return null;
        }
    }

    public function getUser($id){
        $query="SELECT * FROM users WHERE '$id' = user_id";
        $pdo = $this->pdo;

        $stmt = $pdo->prepare($query);
        $ret = $stmt->execute();

        if ($ret) {
            $obj = $stmt->fetchAll();
            return $obj;
        } else {
            return null;
        }
    }
    public function getCourseId($keywords)
    {
        $query = "SELECT DISTINCT id FROM course WHERE ";
        $keyword = array();

        for($i = 0 ; $i < count($keywords) ; $i++)
            $keyword[$i] = "name like \"%" . $keywords . "%\" or code like \"%" . $keywords . "%\"";

        $keyword_result = implode(" or ", $keyword);

        $query = $query . $keyword_result;

        //echo $query;

        $pdo = $this->pdo;

        $stmt = $pdo->prepare($query);
        $ret = $stmt->execute();

        if ($ret) {
            $obj = $stmt->fetchAll();
            /*$id_string = array();
            foreach ($obj as $value) {
                array_push($id_string, $value["id"]);
            }*/
            return $obj;
        } else {
            return null;
        }
    }

    public function getArticleList($course_ids) {
         $query="SELECT article.id, article.course_id, code, course.name AS course_name, year, category.name AS category_name, title, content, startdate, enddate, users.name AS user_name
                                FROM article JOIN (SELECT * FROM revision ORDER BY revision.id DESC) revision JOIN course JOIN category JOIN users
                                ON course_id = course.id AND category_id = category.id AND revision.article_id = article.id AND users.user_id = article.user_id
                                ";
        if ($course_ids) {
            $query .="WHERE article.course_id in (";
            $str = explode(',', $course_ids);
            for($i=0;$i<count($str);$i++){
                if(preg_match("/^[0-9]+$/", $str[$i])) {
                    $query .= $query+$str[$i];
                    if($i<count($str)-1){
                        $query .=", ";
                    }
                } else {
                    $query .="0";
                    break;
                }
            }
            $query .=")";
        }

        $query .= " GROUP BY article.id ORDER BY article.id DESC LIMIT 20";

        $pdo = $this->pdo;

        $stmt = $pdo->prepare($query);
        $ret = $stmt->execute();

        if ($ret) {
            $obj = $stmt->fetchAll();
            return $obj;
        } else {
            return null;
        }

    }
    public function changeUserLevel($id, $value){
        if(preg_match("/^[0-9]{1,2}$/",$value)){
            $query="UPDATE users set level='$value' WHERE level<99 AND '$id' = user_id";
            $pdo = $this->pdo;
            $pdo->beginTransaction();
            $stmt = $pdo->prepare($query);
            $ret = $stmt->execute();
            $pdo->commit();
        }
        else{
            echo
                "<script>
                alert('wrong input.');
                history.back();
                </script>";
        }
    }

    public function deleteUser($id){
        $query="DELETE FROM users WHERE level<99 AND '$id' = user_id";
        $pdo = $this->pdo;
        $pdo->beginTransaction();
        $stmt = $pdo->prepare($query);
        $ret = $stmt->execute();
        $pdo->commit();

    }
    public function editArticle($id, $content, $startdate, $enddate) {
        $pdo = $this->pdo;

        $pdo->beginTransaction();

        $query = "INSERT INTO revision (content, article_id, startdate, enddate) VALUES (:content, :article_id, :startdate, :enddate)";
        $stmt = $pdo->prepare($query);
        $stmt->bindValue(':content', $content);
        $stmt->bindValue(':startdate', $startdate);
        $stmt->bindValue(':enddate', $enddate);
        $stmt->bindValue(':article_id', $id);
        $stmt->execute();

        $pdo->commit();
    }

    public function insertCategory($name) {
        $query = "SELECT * FROM category WHERE '$name'=name";
        $pdo = $this->pdo;
        $stmt = $pdo->prepare($query);
        $ret = $stmt->execute();
        if($ret){
            $check = $stmt->fetch();
            if($check){
                 echo
                "<script>
                alert('already in');
                history.back();
                </script>";
            }
            else{
                $pdo = $this->pdo;

                $pdo->beginTransaction();

                $query = "INSERT INTO category (name) VALUES (:name)";
                $stmt = $pdo->prepare($query);
                $stmt->bindValue(':name', $name);
                $stmt->execute();

                $category = $pdo->query("SELECT id FROM category ORDER BY id DESC LIMIT 1;");
                $rows = $category->fetch();
                $id = $rows["id"];

                $pdo->commit();

                return $id;
            }
        }
        else{
            return null;   
        }
    }
    
    public function insertCourse($code, $name, $year) {
        $query = "SELECT * FROM course WHERE '$code'= code AND '$name' = name AND '$year'=year";
        $pdo = $this->pdo;
        $stmt = $pdo->prepare($query);
        $ret = $stmt->execute();
        if ($ret) {
            
            $check = $stmt->fetch();
            if($check){
                 echo
                "<script>
                alert('already in');
                history.back();
                </script>";
            }
            else{
                $pdo = $this->pdo;
                $pdo->beginTransaction();
                $query = "INSERT INTO course (code,name,year) VALUES (:code,:name,:year)";
                $stmt = $pdo->prepare($query);
                $stmt->bindValue(':code', $code);
                $stmt->bindValue(':name', $name);
                $stmt->bindValue(':year', $year);
                $stmt->execute();

                $category = $pdo->query("SELECT id FROM course ORDER BY id DESC LIMIT 1;");
                $rows = $category->fetch();
                $id = $rows["id"];

                $pdo->commit();

                return $id;
            }
        } else {
            return null;
        }
        
        
    }


    public function deleteArticles($articles) {
        /*
            add by 이정훈 : 게시물 삭제. $articles : 배열.
        */

        $level = $_SESSION['user']['level'];
        $user_id = $_SESSION['user']['user_id'];

        foreach ($articles as $article) {
            $pdo = $this->pdo;
            $pdo->beginTransaction();
            $stmt = $pdo->prepare("DELETE FROM article WHERE id=:id AND ( :level >= 99 OR user_id=:user_id )");
            $stmt->bindValue(':id', $article);
            $stmt->bindValue(':user_id', $user_id);
            $stmt->bindValue(':level', $level);
            $ret = $stmt->execute();

            $pdo->commit();
        }
    }


    public function sendMail($to, $subject, $message) {

        $headers   = array();
        $headers[] = "MIME-Version: 1.0";
        $headers[] = "Content-type: text/plain; charset=iso-8859-1";
        $headers[] = "From: CourseFeed <coursefeed@google.com>";
        $headers[] = "Reply-To: CourseFeed <coursefeed@google.com>";
        $headers[] = "Subject: {$subject}";
        $headers[] = "X-Mailer: PHP/".phpversion();

        $httpmessage = '
        <html>
        <head><title><?=$subject?></title></head>
        <body><p><?=$message?></p></body>
        </html>
        ';

        return mail($to, $subject, $message, implode("\r\n", $headers));
    }

    public function genCertificateCode($to,$user_id) {

        require("mail.php");
        $body = '
        <html>
        <head><title>CourseFeed Certify Email</title></head>
        <body>
        <h1>Thank You for Join us, ';
        $body .= $user_id;
        $body .= '.</h1>
        <p>Just Click ! then, You are Real CourseFeeder.</p>
        <br/>
        <a href="http://localhost:8888/coursefeed/certificate.php?key=';
        $body .= md5($user_id);
        $body .= '">Click ME !</a></body></html>';

        \mail\send($to, $user_id, "CourseFeed Certificate.", $body);
    }

    public function certify($key) {
        $pdo = $this->pdo;
        $pdo->beginTransaction();
        $query="UPDATE users set level=2 WHERE level=1 AND md5(user_id)=:key";
        $stmt = $pdo->prepare($query);
        $stmt->bindValue(':key', $key);
        $ret = $stmt->execute();
        $pdo->commit();

        $query="SELECT name FROM users WHERE level=2 AND md5(user_id)=:key";
        $stmt = $pdo->prepare($query);
        $stmt->bindValue(':key', $key);
        $ret = $stmt->execute();
        if ($ret) {
            $obj = $stmt->fetchAll();
            return $obj;
        } else {
            return null;
        }
    }

}

?>
