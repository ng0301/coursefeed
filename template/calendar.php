<?php

namespace template\calendar;

?>

<? function renderCalendarForm($articles, $month) { ?>
    <? function renderMonthLink($month, $thisMonth=false) { ?>
        <?
            if ($thisMonth) {
                $title = "이번 달";
                $link = \http\query_string($_GET, array('month'=>date('m')));
            } else {
                $title = $month;
                $link = \http\query_string($_GET, array('month'=>$month));
            }

            $cls = "";

            if ($month === date('m')) {
                $cls = "pure-button-disabled";
                $link .= "#";
            }
        ?>
        <? if (validMonth($month)) { ?>
        <a href="article.php<?= $link ?>" class="pure-button pure-button-xsmall <?= $cls ?>"><?= $title ?></a>
        <? } ?>
    <? } ?>
    <?
    $year = date('Y');

    if (!validMonth($month)) {
        $month = date('m');
    }


    $time = strtotime($year.'-'.$month.'-01');
    list($tday, $sweek) = explode('-', date('t-w', $time));  // 총 일수, 시작요일
    $tweek = ceil(($tday + $sweek) / 7);  // 총 주차
    $lweek = date('w', strtotime($year.'-'.$month.'-'.$tday));  // 마지막요일

    ?>
    <h3><?=$year?>년 <?=$month?>월</h3>
    <p>
        <? renderMonthLink($month - 1); ?><? renderMonthLink($month, true); ?><? renderMonthLink($month + 1); ?> View as <a href="article.php<?= \http\query_string($_GET, array('month'=>null))?>">List</a>
    </p>
    <section id="calendar">
    <table id="calendar_table">
        <colgroup>
            <col class="weekend" />
            <col />
            <col />
            <col />
            <col />
            <col />
            <col class="weekend" />
        </colgroup>
        <thead><tr><th>일</th><th>월</th><th>화</th><th>수</th><th>목</th><th>금</th><th>토</th></tr></thead>

        <tbody>
        <? for ($n=1,$i=0; $i<$tweek; $i++): ?>
            <tr>
            <? for ($k=0; $k<7; $k++): ?>
                <td>
                    <? if (($i == 0 && $k < $sweek) || ($i == $tweek-1 && $k > $lweek)){ ?>
                    
                    <? continue;} ?>
                    <p><?=$n++?></p>
                    <ul class="calendar-day">
                    <? foreach ($articles as $article) {
                        $delta_s = strtotime($year."-".$month."-".$n) - strtotime($article['startdate']);
                        $delta_e = strtotime($article['enddate']) - strtotime($year."-".$month."-".$n) + 86401;
                        if ($delta_s>0 and $delta_e > 0 ) { ?>

				            <li class="article">
				            	<a href="article.php?id=<?=$article["id"]?>">
				            	<?=preg_replace('/([\w]{,3})\w*/', 'aaa', $article["course_name"])?>
				            	<span class="category">
				            		<?=htmlspecialchars(mb_substr($article["category_name"], 0, 3, "utf8")) ?>
				            	</span>
				            	</a>
				            </li>

                        <? } ?>
                    <? } ?>
                    </ul>
                </td>
            <? endfor; ?>
            </tr>
        <? endfor; ?>
        </tbody>
    </table>
    </section>

<? } ?>
