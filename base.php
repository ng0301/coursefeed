<?php
//session_start();
require_once("ti.php");
require_once("coursefeed.php");
require_once("template/loginform.php");
require_once("template/searchform.php");
require_once("template/article.php");
require_once("http.php");
$coursefeed = new CourseFeed();

function sanitize_output($buffer) {

    $search = array(
        '/\>[^\S ]+/s',  // strip whitespaces after tags, except space
        '/[^\S ]+\</s',  // strip whitespaces before tags, except space
        '/(\s)+/s'       // shorten multiple whitespace sequences
    );

    $replace = array(
        '>',
        '<',
        '\\1'
    );

    return preg_replace($search, $replace, $buffer);
}
ob_start("sanitize_output");
?>
<html>
<head>
    <title>CourseFeed</title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <link rel="stylesheet" type="text/css" href="pure.css" />
    <link rel="stylesheet" type="text/css" href="pure-extend.css" />
    <link rel="stylesheet" type="text/css" href="style.css" />
    <link rel="stylesheet" type="text/css" href="//netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.css" />
    <script type="text/javascript" src="./js/prototype.js"></script>
    <script type="text/javascript" src="./js/coursefeed.js"></script>
    <script type="text/javascript" src="./js/AutoComplete.js"></script>
    <script type="text/javascript" src="./js/scriptaculous.js"></script>
</head>
<body>
    <section id="menu">
        <header>
            <h1><a href="."><img src="img/logo.png" alt="CourseFeed" /></a></h1>
        </header>
        <nav>
            <?php
            $coursefeed = new CourseFeed();
            if ($coursefeed->isLoggedIn()) {
                template\loginform\renderLoginForm($_SESSION['user']);
            } else {
                template\loginform\renderLoginForm(null);
            }

            ?>
        </nav>
        <nav>
            <p>
                <?php
                    template\searchform\renderSearchForm();
                ?>
            </p>
        </nav>
        <footer>
            <p>CourseFeed, 2013.</p>
        </footer>
    </section>
    <section id="content">
    <?php emptyblock('content') ?>
    </section>
</body>
</html>
