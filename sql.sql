DROP TABLE IF EXISTS upvote;
DROP TABLE IF EXISTS revision;
DROP TABLE IF EXISTS article;
DROP TABLE IF EXISTS category;
DROP TABLE IF EXISTS course;
DROP TABLE IF EXISTS users;


CREATE TABLE category (
id		int AUTO_INCREMENT PRIMARY KEY,
name	varchar(20) NOT NULL,
    UNIQUE(name)
) ENGINE=innodb;



CREATE TABLE course (
id		int AUTO_INCREMENT PRIMARY KEY,
code	char(8),
name	varchar(40),
year	int,
    UNIQUE(code,name,year)
) ENGINE=innodb;


CREATE TABLE article (
    id int AUTO_INCREMENT PRIMARY KEY,
    title varchar(20) NOT NULL,
    category_id	int,
    course_id   int,
    user_id     varchar(16),
    FOREIGN KEY (category_id) REFERENCES category(id),
    FOREIGN KEY (course_id) REFERENCES course(id)
) ENGINE=innodb;

CREATE TABLE revision (
    id int AUTO_INCREMENT PRIMARY KEY,
    content text NOT NULL,
    article_id int NOT NULL,
    uploaded_at timestamp,
    startdate   date,
    enddate     date,
    FOREIGN KEY (article_id) REFERENCES article(id) ON DELETE CASCADE
) ENGINE=innodb;

CREATE TABLE users (
    user_id varchar(16) PRIMARY KEY,
    password varchar(32) NOT NULL,
    name varchar(20) NOT NULL,
    email varchar(40) NOT NULL,
    level tinyint DEFAULT 1,
    fame int DEFAULT 0
) ENGINE=innodb;

CREATE TABLE upvote (
    user_id varchar(16) NOT NULL,
    article_id int NOT NULL,
    FOREIGN KEY (user_id) REFERENCES users(user_id),
    FOREIGN KEY (article_id) REFERENCES article(id) on DELETE CASCADE,
    UNIQUE (article_id, user_id)
) ENGINE=innodb;
