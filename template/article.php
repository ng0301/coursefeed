<?php

namespace template\article;

function renderContent($text) {
    return \mb_ereg_replace("\\n", "<br />", htmlspecialchars($text));
}

?>

<? function renderEntry($article) { ?>
    <? global $coursefeed; ?>
    <h2><?= htmlspecialchars($article["title"]) ?></h2>
    <div class="pure-g">

        <div class="pure-u-2-3">
            <section class="article-entry">
            <p><span class="course"><?= $article["course_name"]?></span> <span class="category"><?= $article["category_name"]?></span> <span class="timestamp"><?= $article["latest_revision"]["uploaded_at"] ?></span></p>
            <p><?= renderContent($article["latest_revision"]["content"]) ?></p>
            <p>
                <?=$article["latest_revision"]["startdate"]?>
                <? if (array_key_exists("enddate", $article["latest_revision"])) { ?>
                    ~ <?=$article["latest_revision"]["enddate"] ?>
                <? } ?>
            </p>

            <p>by <?= htmlspecialchars($article["user_name"]) ?><br />
                </p>
            </section>

            <p class="upvotes">
                <button class="pure-button pure-button-xsmall <? if (!$article["can_upvote"] && $coursefeed->isLoggedIn()) { ?>pure-button-success<? } ?>" <? if (!$article["can_upvote"]) { ?>disabled="disabled" <? } ?> data-article-id="<?= $article['id'] ?>">
                    <i class="fa fa-thumbs-up"></i> <span><? if (!$article["can_upvote"] && $coursefeed->isLoggedIn()) { ?>upvoted<? } else { ?>upvote<? } ?></span>
                </button>
                <span class="count"><?= $article["upvotes"] ?></span>
            </p>
            <p>
            <? if( isset($_SESSION['user'])) { ?>
                <? if( $_SESSION['user']['user_id'] == $article['user_id']) { ?>
                <a href="article_edit.php?id=<?=$article["id"]?>">edit</a> |
                <? } if( $_SESSION['user']['user_id'] == $article['user_id'] || $_SESSION['user']['level'] >= 99) { ?>
                <a href="article_delete.php?id=<?=$article["id"]?>">delete</a> |
                <? } ?>
            <? } ?>
                <a href="article.php<?= \http\query_string($_GET, array("id"=>null))?>">list</a>
            </p>
        </div>
        <div class="pure-u-1-3">
            <? if (count($article["revisions"]) > 0) { ?>
                <h3>revisions</h3>
                <ol>
                <? foreach ($article["revisions"] as $revision) { ?>
                    <li class="revision">
                        <p><?= renderContent($revision["content"]) ?></p>
                        <p><?=$article["latest_revision"]["startdate"]?>
                        <? if (array_key_exists("enddate", $article["latest_revision"])) { ?>
                        ~ <?=$article["latest_revision"]["enddate"] ?>
                        <? } ?></p>
                        <p class="timestamp">at <?= $revision["uploaded_at"] ?></p>
                    </li>
                <? } ?>
                </ol>
            <? } ?>
        </div>
    </div>
<? } ?>

<? function renderNotFound() { ?>
    <h2>Article Not Found</h2>
    <p>Quite embarrasing, isn't it?</p>
<? }?>

<? function renderInsertForm($courses, $categories) { ?>
<h2>Write a Article</h2>
 <form method="post" class="article pure-form pure-form-stacked">
    <div><label>Title<input id="title" name="title" type="text" /></label></div>
    <div><label>Courses <a href="course_edit.php"><i class="fa fa-plus"></i></a><select name="course">
        <? foreach ($courses as $course) {?>
            <option value="<?=$course['id']?>">[<?=$course['code']?>] <?= htmlspecialchars($course['name'])?> <?=$course['year']?></option>
        <? } ?>
    </select></label></div>

    <div><label>Categories<select name="category">
        <? foreach ($categories as $category_id) {?>
            <option value="<?=$category_id['id']?>"><?=htmlspecialchars($category_id['name'])?></option>
        <? } ?>
    </select></label></div>

    <!-- 2013.11.15 added by 이정훈 - 글쓰기에 date form 추가. -->
    <div><label>Start Date<input type="date" id="startdate" name="startdate" value="<?=date('Y-m-d')?>"></label></div>
    <div><label>End Date<input type="date" id="enddate" name="enddate" value="<?=date('Y-m-d')?>"></label></div>

    <div><label>Content<textarea id="contentarea" name="content" cols="80" rows="5"></textarea></label></div>
    <div><input type="submit" value="write" id="submit" class="pure-button pure-button-primary" /> | <a href="article.php<?= \http\query_string($_GET, array("id"=>null))?>">list</a></div>
</form>
<? } ?>

<? function renderEditForm($article) { ?>
    <h2>Edit a Article</h2>
    <h3><?= htmlspecialchars($article["title"]) ?></h3>
    <section class="article-entry">
    <p><span class="course"><?= $article["course_name"]?></span> <span class="category"><?= $article["category_name"]?></span> <span class="timestamp"><?= $article["latest_revision"]["uploaded_at"] ?></span></p>

    <form method="post" class="article pure-form pure-form-stacked">
    <input type="hidden" name="id" value="<?= $article["id"] ?>" />
    <!-- 2013.11.15 added by 이정훈 - 글수에 date form 추가. -->
    <div><label>StartDate<input type="date" id="startdate" name="startdate" value="<?= $article["latest_revision"]["startdate"] ?>" /></label></div>
    <div><label>EndDate<input type="date" id="enddate" name="enddate" value="<?= $article["latest_revision"]["enddate"] ?>"></label></div>
    <div><label>Content<textarea name="content" cols="80" rows="5"><?= htmlspecialchars($article["latest_revision"]["content"]) ?></textarea></label></div>
    <div><input type="submit" value="edit" id="submit" class="pure-button pure-button-primary" /> | <a href="article.php<?= \http\query_string($_GET, array("id"=>null))?>">list</a></div>
    </form>
    </section>
<? } ?>

<!-- add by 이정훈 delete 확 -->
<? function renderDeleteForm($articles) { ?>
 <form method="post" class="article pure-form pure-form-stacked">
    <h2>Are you sure ?</h2>
    <p>Delete # <?=implode(",", $articles)?></p>
    <? foreach ($articles as $article) { ?>
        <span></span>
        <input type="hidden" name="articles[]" value="<?=$article?>">
    <? } ?>
    <div><input type="submit" value="Delete" name="delete" class="pure-button pure-button-warning" /> <input type="submit" value="Back" name="back" class="pure-button" /></div>
</form>
<? } ?>
