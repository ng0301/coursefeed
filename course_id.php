<?php

require_once("coursefeed.php");
require_once("http.php");

$coursefeed = new CourseFeed();

ob_start();
header("Content-type: application/json");

function printJsonCourseId($ids) {
    print json_encode(array("id"=>$ids));
}

if ($_SERVER['REQUEST_METHOD'] == "GET") {
    if (http\has_parameter($_GET, "keyword")) {
    	$result = $coursefeed->getCourseId($_GET["keyword"]);
    	$string = array();
    	foreach ($result as $value) {
    		array_push($string, $value["id"]);
    	}
        printJsonCourseId( $string );
        # array(array("id" => 1, "code"=>"CSE326"))
        header('HTTP/1.1 200 OK');
    } else {
        header('HTTP/1.1 400 Bad Request');
    }
} else {
    header('HTTP/1.1 405 Method Not Allowed');
}

ob_end_flush();
exit;

?>
