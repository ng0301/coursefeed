var id_flag = false;

function formValidate() {
	var $start = $("startdate");
	var $end = $("enddate");
    if (!($start !== null && $end !== null)) {
        return;
    }

	var $valid = false;
	// date validate
	if($end.value < $start.value){
		$end.setStyle({
			backgroundColor: '#ffdddd',
			border : 'solid red 1px'
		});
		$valid = true;
	} else {
		$end.setStyle({
			backgroundColor: '#ffffff',
			border : 'none'
		});
		$valid = false;
	}
	// for validate
	$valid = ($("title") === null || $("title").value !== "") &&
             ($("contentarea") === null || $("contentarea").value !== "");
    if ($$("form.article #submit")) {
        $$("form.article #submit")[0].disabled = !$valid;
    }
}

function upvoteSuccess(ajax) {
    var obj = JSON.parse(ajax.responseText);
    $$(".upvotes span.count")[0].update(obj.upvotes);
    $$(".upvotes button")[0].disabled = "disabled";
    $$(".upvotes button")[0].addClassName("pure-button-success");
    $$(".upvotes button span")[0].update("upvoted");
}

function upvote(event) {
    var element = event.findElement();
    var article_id = element.readAttribute('data-article-id');
    new Ajax.Request("upvote.php", {
        parameters: {article_id: article_id},
        onSuccess: upvoteSuccess
    });
}


function checkIdRedundancy()
{
    var id = $$('#joinform input.id')[0].value;
    new Ajax.Request("id_check.php",
                     {method: "post",
                      parameters: {id: id},
                      onSuccess: passIdCheck,
                      onFailure: failIdCheck,
                      onException: failIdCheck}
                    );
}

function passIdCheck(ajax)
{
    $$('#joinform input.id')[0].removeClassName("id-check-fail");
    $$('#joinform input.id')[0].addClassName("id-check-pass");
    id_flag = false;
}

function failIdCheck(ajax)
{
    $$('#joinform input.id')[0].removeClassName("id-check-pass");
    $$('#joinform input.id')[0].addClassName("id-check-fail");
    id_flag = true;
}

function checkFillInput()
{
    if( !$$('#joinform input')[0].value.match(/[\w]{4,20}$/)
        || !$$('#joinform input')[1].value.match(/^.{4,20}$/)
        || !$$('#joinform input')[2].value.match(/^.{1,40}$/)
        || id_flag)
        $$('#joinform input.joinButton')[0].disabled = "disabled";
    else
        $$('#joinform input.joinButton')[0].disabled = "";
}

function storeCourseId(ajax)
{
    var obj = JSON.parse(ajax.responseText);
    if( obj.id != "")
    {
        var url = "article.php?course_id=" + obj.id;
        location.href = url;
    }
    else
    {
        alert("검색 결과가 없습니다.");
    }
}

function searchArticle()
{
    var keyword = $$('#searchform input')[0].value;
    if(keyword == "")
        return 0;

    new Ajax.Request("course_id.php",
                     {method: "GET",
                      parameters: {keyword: keyword},
                      onSuccess: storeCourseId});
}

function showMore () {
    $("article-list").insert();
}

window.onload = function() {
    if ($("startdate")) {
       $("startdate").observe("change", formValidate);
    }
    if ($("enddate")) {
        $("enddate").observe("change", formValidate);
    }

    if ($("title")) {
        $("title").observe("keyup",formValidate);
    }

    if ($("contentarea")) {
        $("contentarea").observe("keyup",formValidate);
    }

    if ($$(".upvotes button").length > 0) {
        $$(".upvotes button")[0].observe("click", upvote);
    }

    if ($$('#joinform input.id').length > 0) {
        $$('#joinform input.id')[0].observe("keyup",checkIdRedundancy);
         checkFillInput();
    }

    if( $('joinform') ){
        $('joinform').observe("change", checkFillInput);
        $('joinform').observe("keyup", checkFillInput);

        checkFillInput();
    }

    if( $('searchform') ){
        $$('#searchform button')[0].observe("click", searchArticle);
    }

    if ($$('form.article').length > 0) {
        formValidate();
    }
}
